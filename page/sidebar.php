<div id="pagebodysub">

<p><a href="index.php">POSレジ管理画面トップ</a></p>
<p><a href="posfront.php">
	<img 
	src="../css/s_posfront.jpg" 
	onmouseover="this.src='../css/s_posfront_2.jpg';" 
	onmouseout="this.src='../css/s_posfront.jpg';" height="32" width="190">
</a></p>

<div id="acc">
<h3>POSレジの管理</h3>
<div class="pulldown_menu">
<ul class="side_list">
<li><a href="posstart.php">開始レジ金の設定</a></li>
<li><a href="posmovelist.php">レジ金移動の一覧</a></li>
<li><a href="posfinish.php">レジの締め処理</a></li>
</ul>
</div>
<h3>売り上げデータの集計</h3>
<div class="pulldown_menu">
<ul class="side_list">
<li><a href="posreglist.php">レジの売り上げ一覧</a></li>
<li><a href="shopreport.php">月日別の集計</a></li>
<li><a href="itemreport.php">商品別の集計</a></li>
<li><a href="categoryreport.php">商品カテゴリ別の集計</a></li>
</ul>
</div>
<h3>マスターの管理</h3>
<div class="pulldown_menu">
<ul class="side_list">
<li><a href="customerlist.php">会員の一覧</a></li>
</ul>
</div>
<h3>店舗の管理</h3>
<div class="pulldown_menu">
<ul class="side_list">
<li><a href="posshop.php">基本情報の編集</a></li>
<li><a href="staff.php">スタッフ一覧</a></li>
</ul>
</div>
</div>


</div>