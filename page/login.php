<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content= "text" ; charset="UTF-8">
<title>POS|ログイン</title>
	<link type="text/css" rel="stylesheet" href="../css/style_log.css">
</head>
<body id="top">
<div id="main">
<div id="log_s">
<img class="logo" src="../css/login_logo.gif">
</div>
	<?php
	   if(isset($_COOKIE['id'])){
    $_POST['save'] = 'on';
    $_SESSION['name'] = $_COOKIE['name'];
    header('location: index.php');
  }
  ?>
<div id="log_m">
 	<ul style="list-style:none;">
	<form action="login_p.php" method="post" name="login_info" autocomplete="off" >
	<li><img src="../css/id.gif">
	<input type="text" name="id"  maxlength="100" /></li>
	<li><img src="../css/pass.gif">
	<input type="password" name="pass"  maxlength="100"/></li>
	<li>
	<span style="margin-right: 79px;"></span>
	<input id="save" type="checkbox" name="save" value="on">
	<label for="save"><font color="white">次回から自動でログイン</font></label></li>
	<li>
	<span style="margin-right: 79px;"></span>
	<input type="image" src="../css/login_btn.gif" onmouseover="this.src='../css/login_btn_2.gif'" onmouseout="this.src='../css/login_btn.gif'"></li>
    </form>
</ul>
</div><!--main-->
<div id="footer">
管理画面の対応ブラウザは、Microsoft(R) Internet Explorer(R) 7.0 以上です<br />
ブラウザのポップアップブロッカーは解除してご利用ください。<br />

<p>Copyright&copy; <?php echo date('Y'); ?> <?php echo "Paris de Skin" ?> . All rights reserved.
</div>
</div>
</body>
</html>