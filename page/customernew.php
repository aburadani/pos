<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<div id="pagebodymain">
<h1>新しく会員を登録</h1>
<table>
	<caption>会員の情報</caption>
	<tr>
		<th class="a">会員番号</th>
		<th>入力フォーム</th>
	</tr>
	<tr>
		<th class="a">会員名<font color="red">(*)</font></th>
		<th>姓　入力
			名　入力</th>
	</tr>
	<tr>
		<th class="a">会員名(フリガナ)<font color="red">(*)</font></th>
		<th>セイ　入力
			メイ　入力</th>
	</tr>
	<tr>
		<th class="a" rowspan="5">住所</th>
		<th>郵便番号　入力</th>
	</tr>
	<tr>
		<th>都道府県 プルダウン</th>
	</tr>
	<tr>
		<th>市区町村　入力</th>
	</tr>
	<tr>
		<th>番地等</th>
	</tr>
	<tr>
		<th>建物名等</th>
	</tr>
	<tr>
		<th class="a">電話番号1<font color="red">(*)</font></th>
		<th>入力</th>
	</tr>
	<tr>
		<th class="a">電話番号2<font color="red">(*)</font></th>
		<th>入力</th>
	</tr>
	<tr>
		<th class="a">ファックス番号</th>
		<th>入力</th>
	</tr>
	<tr>
		<th class="a">メールアドレス</th>
		<th>入力</th>
	</tr>
	<tr>
		<th class="a">性別<font color="red">(*)</font></th>
		<th>入力</th>
	</tr>
	<tr>
		<th class="a">生年月日<font color="red">(*)</font></th>
		<th>プルダウン</th>
	</tr>
	<tr>
		<th class="a">メルマガ</th>
		<th>ラジオボタン</th>
	</tr>
	<tr>
		<th class="a">DM</th>
		<th>ラジオボタン</th>
	</tr>
	<tr>
		<th class="a">来店動機</th>
		<th>ラジオボタン</th>
	</tr>
	<tr>
		<th class="a">保有チケット</th>
		<th>枚</th>
	</tr>
	<tr>
		<th class="a">会員レベル</th>
		<th>ラジオボタン</th>
	</tr>
	<tr>
		<th class="a">運営者用のメモ</th>
		<th>入力</th>
	</tr>
	<tr>
		<th class="a">退会処理</th>
		<th>ラジオボタン</th>
	</tr>
</table>
<p>
<table>
	<caption>利用状況</caption>
	<tr>
		<th class="a">登録日時</th>
		<th>表示</th>
		<th class="a">更新日時</th>
		<th>表示</th>
	</tr>
	<tr>
		<th class="a">最終購入日時</th>
		<th>表示</th>
		<th class="a">退会日時</th>
		<th>表示</th>
	</tr>
	<tr>
		<th class="a">利用回数</th>
		<th>表示</th>
		<th class="a">購入数量</th>
		<th>表示</th>
	</tr>
	<tr>
		<th class="a">購入金額</th>
		<th>表示</th>
		<th class="a">使用ポイント</th>
		<th>表示</th>
	</tr>
</table>
<p>
<font color="red">(*)は必須項目です。</font>
<p>
	入力内容の確認　上記の内容で確認




</div>
<?php include("footer.php"); ?>
