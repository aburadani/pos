<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<div id="pagebodymain">
<h1>レジの締め処理</h1>
<p>
	<table>
		<tr>
			<th colspan="4" class="b">基本情報</th>
		<tr> 
			<th class="a">店舗</th>
			<th colspan="3">店舗名表示<th>
		<tr>
			<th class="a">対象日時</th>
			<th>開始の時間表示？</th>
			<th class="a">処理日時</th>
			<th><?php include("year.php"); ?></th>
		<tr>
			<th class="a">スタッフ番号</th>
			<th>プルダウン</th>
			<th class="a">担当者名</th>
			<th>表示</th>
	</table>
<p>
	<table>
		<tr>
			<th colspan="5" class="b">レジ処理の集計</th>
		<tr>
			<th class="a">件数</th>
			<th class="a">点数</th>
			<th class="a">現金</th>
			<th class="a">クレジット</th>
			<th class="a">使用チケット</th>
		<tr><!--現在のレジ状況?-->
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
	</table>
</p>
<p>
	<table>
		<tr>
			<th colspan="4" class="b">レジ金の移動</th>
		<tr>
			<th class="a">日時</th>
			<th class="a">金額</th>
			<th class="a">担当者</th>
			<th class="a">備考</th>
		<tr><!--現在のレジ状況?-->
			<th></th>
			<th></th>
			<th></th>
			<th>移動合計金額: *円</th>
	</table>
</p>
<p>
	<table>
		<tr>
			<th colspan="6" class="b">レジ金の集計</th>
		<tr>
			<th class="a">開始レジ金</th>
			<th>**円</th>
			<th class="a">理論レジ金</th>
			<th>**円</th>
			<th class="a">実レジ金</th>
			<th>（入力）円</th>
		<tr>
			<th class="a">預かり</th>
			<th>**円</th>
			<th class="a">お釣り</th>
			<th>**円</th>
			<th class="a">レジ金の差額</th>
			<th>**円</th>
	</table>
</p>
<p style="text-align: right;">入力内容の確認</p>



</div>
<?php include("footer.php"); ?>
