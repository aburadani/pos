<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<div id="pagebodymain">
<h1>基本情報の編集</h1>
<p>
	<table>
		<tr>
			<th class="a">店舗名<font color="#ff0000">(*)</font></th>
			<th>店舗の名前表示＋変更できるように</th>
		<tr>
			<th class="a">略称<font color="#ff0000">(*)</font></th>
			<th>略称表示＋変更できるように</th>
		<tr>
			<th class="a">店舗ID</th>
			<th>店舗ID表示(店舗IDは変更できません)</th>
		<tr>
			<th class="a">パスワード</th>
			<th>パスワード表示＋（必ず半角英数字で入力してください。４文字以上を推奨します。）</th>
		<tr>
			<th class="a">店舗管理者メールアドレス</th>
			<th>入力フォーム</br>
				,(半角カンマ)で区切っていただくことで複数のメールアドレスが登録可能です</th>
		<tr>
			<th class="a">開始レジ金のメッセージ</th>
			<th><!--プルダウン（時間）-->時（開始レジ金のエラーメッセージを表示する時間）</th>
		<tr>
			<th class="a">締め処理のメッセージ</th>
			<th><!--プルダウン（時間）-->時（締め処理のエラーメッセージを表示する時間）
	</table>
<p>
<font color="#ff0000">(*)</font>は必須項目です。
<p>
	入力内容の確認　上記の内容で登録
</div>
<?php include("footer.php"); ?>
