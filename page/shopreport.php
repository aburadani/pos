<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<script>
	function kouji(){
		alert("工事中です");
	}
	function serch(id,num){
		var year_st = $('#year_s').val();
		var month_st = $('#month_s').val();
		var day_st = $('#day_s').val();
		var year_en = $('#year_e').val();
		var month_en = $('#month_e').val();
		var day_en = $('#day_e').val();
		var day_or_month = $("input:radio[name='dayormonth']:checked").val();
		//day_or_month : 日別表示のときは0,月別表示のときは1
		var sort = $("input:radio[name='day_or_sales']:checked").val();
		//day_or_sales : 日付順の時は0,売上順のときは1
		var staff_id = $('#staff').val();
		$("tr[name='add']").remove();
		$("input[name='add_button']").remove();
				$.post('shop_log.php',{key_id:id,
									   key_staff_id:staff_id,
									   key_sort:sort,
									   key_day_or_month:day_or_month,
									   key_span:num,
									   key_year_st:year_st,
									   key_month_st:month_st,
									   key_day_st:day_st,
									   key_year_en:year_en,
									   key_month_en:month_en,
									   key_day_en:day_en},
									   function(data){$('#table').append(data);}
									   );
				$.post('log_span.php',{key_id:id,
									   key_staff_id:staff_id,
								   	   key_span:num,
								   	   key_day_or_month:day_or_month,
									   key_year_st:year_st,
									   key_month_st:month_st,
									   key_day_st:day_st,
									   key_year_en:year_en,
									   key_month_en:month_en,
									   key_day_en:day_en},
									   function(data){$('#span').append(data);}
									   );	
		
	}
	$(document).ready(function(){   
    $("#staff").keyup(function (e) {
        var str = $("#staff").val();        
      	//strの中身をphpに飛ばす
      	$('#staffname').load('get_staff_name.php',{id:str});
    });
	
  });
</script>
<div id="pagebodymain">
<h1>月日別の集計</h1>
<p>
	<table>
		<tr>
		<th colspan="4" class="b">検索条件</th>
		<tr>
			<th class="a">店舗</th>
			<th><?php echo $_SESSION['name'];?></th>
			<th class="a">集計対象</th>
			<th>
			<input type="radio" value="0" name="dayormonth" checked="checked">日別
			<input type="radio" value="1" name="dayormonth">月別
			</th>
		</tr>
		<tr>
			<th class="a">スタッフ番号</th>
			<th><input type="text" value="" id="staff"></th>
			<th class="a">担当者名</th>
			<th id="staffname"><!--担当者名表示--></th>
		</tr>
		<tr>
			<th class="a">表示順</th>
			<th colspan="3" align="left">
				<input type="radio" name="day_or_sales" value="0" checked="checked">日付順
				<input type="radio" name="day_or_sales" value="1">売り上げ順
			</th>
		<tr>
			<th>
				集計期間
			</th>
			<th colspan="3">
				<select id="year_s" >
					<?php
						$year_s = date("Y");
						for($i=2010;$i<=$year_s;$i++){
						if($i == $year_s){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>年
				<select id="month_s">
					<?php
						$month_s = date("n");
						for($i=1;$i<=12;$i++){
						if($i == $month_s){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>月
				<select id="day_s">
					<?php
						$day_s = date("j");
						for($i=1;$i<=31;$i++){
						if($i == 1){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>日  から
					<select id="year_e" >
					<?php
						$year_e = date("Y");
						for($i=2010;$i<=$year_e;$i++){
						if($i == $year_e){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>年
				<select id="month_e">
					<?php
						$month_e = date("n");
						for($i=1;$i<=12;$i++){
						if($i == $month_e){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>月
				<select id="day_e">
					<?php
						$day_e = date("j");
						for($i=1;$i<=31;$i++){
						if($i == $day_e){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>日
			</th>
		</tr>
		<tr>
			<th colspan="4">
				<input type="image" src="../css/contents_img/search_reset.gif" onclick="location.reload();">
				<input type="image" src="../css/contents_img/search.gif" onclick=<?php
				$shop_id = $_SESSION['id'];
				echo '"serch('.'\''.$shop_id.'\''.',1);"';
				?>></th>
	</table>
<P><a href="#" onclick="kouji();"><img src="../css/contents_img/csv_btn.gif"></a></p>
<p>
	<table id="table">
		<tr class="a">
			<th>年月日</th>
			<th>点数</th>
			<th>現金</th>
			<th>ｸﾚｼﾞｯﾄ</th>
			<th>割引</th>
			<th>ﾁｹｯﾄ</th>
			<th>返金</th>
			<th>売上金額</th>
			<th>日報</th>
		</tr>
		<?php
	$id = $_SESSION['id'];;
	$year_st = date("Y");
	$month_st = date("n");
	$day_st = 1;
	$year_en = date("Y");
	$month_en = date("n");
	$day_en = 31;//正常にインデックスするようにするため
	$sum=0;
	$day="";
	$str="";
		
	$db_host="localhost";
	$db_user="admin";
	$db_password="";
	$db = mysql_connect($db_host,$db_user,$db_password);
	if(!$db){
		die('DB接続失敗　連絡をお願いします');
	}
	$db_name = $id;//セッションidで指定されたデータベースに接続
	$db_check = mysql_select_db($db_name,$db);
	if(!$db_check){
		echo "DB選択ミス 連絡をお願いします";
	}
	if($id != ""){
		/*対象となるSQL文*/
		$sql = "SELECT SUM(price), SUM(goods_num), DATE_FORMAT(day,'%Y年%m月%d日')as time , SUM(IF (cash_or_card = '1' ,price, '0' ))as cash , SUM(IF (cash_or_card ='2' ,price, '0')) as card FROM shop_log WHERE day >= '".$year_st."-".$month_st."-".$day_st."' and day <= '".$year_en."-".$month_en."-".$day_en."' group by time order by day DESC";
		$rs = mysql_query($sql);
		if(!$rs){
			die('クエリ失敗 連絡をおねがいします');
			}
		while(($arr_item = mysql_fetch_assoc($rs)))
			{//すべての行を処理
			 //日にちごとに合計を算出
			$day = $arr_item['time']; 
			$sum = $arr_item['SUM(price)'];
			$count = $arr_item['SUM(goods_num)'];
			$cash = $arr_item['cash'];
			$card = $arr_item['card'];
			$str .= '<tr name="add"><th>'.$day.'</th><th>'.$count.'</th><th>'.$cash.'</th><th>'.$card.'</th><th></th><th></th><th>0</th><th>'.$sum.'</th><th></th></tr>';
			/*挿入先テーブル情報*/
			/*<tr>
			<th>年月日</th>
			<th>点数</th>
			<th>現金</th>
			<th>ｸﾚｼﾞｯﾄ</th>
			<th>割引</th>
			<th>ﾁｹｯﾄ</th>
			<th>返金</th>
			<th>売上金額</th>
			<th>日報</th>
			</tr>*/
			}
			}
if($day==""){
	echo '<tr name="add"><th colspan="9">対象データがありません</th></tr>';
}else{
echo $str;
}
?>
</table>
<div id="span" align="center">
</div>
<p style="text-align: right;">
	<a href="#" onclick="kouji();"><img src="../css/contents_img/btn_nippou.gif"></a></p>
</div>
<?php include("footer.php"); ?>
