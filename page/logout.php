<?php
session_start();

//セッション情報を削除
$_SESSION = array();
if(ini_get("session.use_cookies")){
	$params = session_get_cookie_params();
	setcookie(session_name(), '', time() - 42000,
		$params['path'], $params['domain'], $params['secure'], $params['httponly']);
}

session_destroy();

//cookie情報を削除
setcookie('id', '', time()-3600);
setcookie('pass', '', time()-3600);

header('location: login.php');
exit();
?>