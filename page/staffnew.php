<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<div id="pagebodymain">
	<h1>
	<div style="text-align:left; float:left;">新しくスタッフを登録</div>
	<div style="text-align:right;"><a href="staff.php" style="margin-right:10px;">スタッフの一覧へ</a></div>
</h1>
<p>
	<table>
		<tr>
			<th class="b" colspan="2">スタッフの情報</th>
		</tr>
		<tr>
			<th class="a">スタッフ番号<font color="red">(*)</font></th>
			<th>(半角4文字以上20文字まで)</th>
		<tr>
			<th class="a">スタッフ名<font color="red">(*)</font></th>
			<th></th>
		</tr>
		<tr>
			<th class="a">フリガナ<font color="red">(*)</font></th>
			<th></th>
		</tr>
		<tr>
			<th class="a">略称<font color="red">(*)</font></th>
			<th></th>
		</tr>
		<tr>
			<th class="a">性別</th>
			<th></th>
		</tr>
	</table>
	<P>
		<table>
			<tr>
				<th class="b" colspan="2">その他</th>
			</tr>
			<tr>
				<th class="a">備考欄</th>
				<th></th>
			</tr>
			<tr>
				<th class="a">一時停止</th>
				<th>一時的にスタッフを停止する</th>
			</tr>
			</table>
	<p>
			<font color="red">(*)</font>は必須項目です。
<p style="text-align:center">
<img src="../css/contents_img/view.gif">
<img src="../css/contents_img/record.gif">

</div>
<?php include("footer.php"); ?>