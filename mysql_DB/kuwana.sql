-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2014 年 6 月 28 日 15:25
-- サーバのバージョン： 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kuwana`
--
CREATE DATABASE IF NOT EXISTS `kuwana` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `kuwana`;

-- --------------------------------------------------------

--
-- テーブルの構造 `goods`
--

DROP TABLE IF EXISTS `goods`;
CREATE TABLE IF NOT EXISTS `goods` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `goods`
--

INSERT INTO `goods` (`id`, `name`, `price`) VALUES
(101001, '両ワキ', 1000),
(101002, '両手の甲', 1000),
(101003, '両足の甲', 1000),
(101004, '両ヒザ', 1000),
(101005, 'おでこ', 1000),
(101006, 'ほほ', 1000),
(101007, '鼻下', 1000),
(101008, '口下', 1000),
(101009, 'おへそ周り', 1000),
(101010, '乳輪周り', 1000),
(102001, '両ひじ下', 2000),
(102003, '胸', 2000),
(102004, 'えり足', 2000),
(102002, '両ひじ上', 2000),
(102005, '腰', 2000),
(102006, 'Vゾーン', 2000),
(102007, '腹上', 2000),
(102008, '腰下', 2000),
(103001, '背中上', 3000),
(103002, '背中下', 3000),
(103003, 'ヒップ', 3000),
(103004, 'Iゾーン', 3000),
(103005, 'Oゾーン', 3000),
(103006, '両ひざ下', 3000),
(103007, '両ひざ下', 3000),
(103008, '顔全体', 3000),
(105001, 'モロッコ溶岩パック', 4000),
(105002, '海藻海泥パック', 4000),
(105003, '光バストアップ', 5000),
(105004, '光ハンドエステ', 3000),
(105005, '光フェイシャルエステ', 10000),
(105007, '脱毛&パックセットコース', 6000),
(105008, '光フェイシャル&パックセットコース', 12000),
(105101, 'ワキパック　単品', 1500),
(105102, 'ワキパック・脱毛セット', 2000),
(105103, 'ヒザパック　単品', 1500),
(105104, 'ヒザパック・脱毛セット', 1985),
(105105, 'パラオパック', 5000),
(105111, 'VCセラム＋ホットパック', 4000),
(105112, 'プラセンコンプレックス＋ホットパック', 4000),
(105113, '透輝の滴＋ホットパック', 4000),
(106005, '5000円セット', 5000),
(301001, 'ブライダルコース', 18000),
(401001, '10000円チケット', 10000),
(401002, '20000円チケット', 20000),
(401003, '30000円チケット', 30000),
(401004, '50000円チケット', 50000),
(500001, '化粧品', 0),
(601020, '上まつげ20本', 3000),
(601040, '上まつげ40本', 4000),
(601060, '上まつげ60本', 5000),
(601080, '上まつげ80本', 6000),
(601100, '上まつげ100本', 7000),
(601120, '上まつげ120本', 8000),
(601140, '上まつげ140本', 9000),
(602010, '下まつげ10本', 2000),
(602020, '下まつげ20本', 3000),
(602030, '下まつげ30本', 4000),
(603001, 'カラー(上まつげ)', 100),
(604001, 'オフ', 1000),
(2000001, '痩身', 0),
(9000001, 'その他', 0),
(9000002, '指名料', 500),
(9990001, '社員割引-施術', 0),
(9990002, '社員割引-販売', 0);

-- --------------------------------------------------------

--
-- テーブルの構造 `guest`
--

DROP TABLE IF EXISTS `guest`;
CREATE TABLE IF NOT EXISTS `guest` (
  `GuestName_kanji` varchar(20) DEFAULT NULL,
  `GuestName_Katakana` varchar(30) DEFAULT NULL,
  `GuestId` int(11) DEFAULT NULL,
  `Tickets` int(11) NOT NULL DEFAULT '0',
  `shop_id` int(11) NOT NULL,
  `test2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `guest`
--

INSERT INTO `guest` (`GuestName_kanji`, `GuestName_Katakana`, `GuestId`, `Tickets`, `shop_id`, `test2`) VALUES
('油谷　創', 'アブラダニ　ソウ', 1, 0, 0, 0),
('高田優人', 'タカダユウト', 2, 0, 0, 0);

-- --------------------------------------------------------

--
-- テーブルの構造 `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `staff_id` int(11) DEFAULT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL,
  `goods_num` int(11) DEFAULT NULL COMMENT '個数',
  `price` int(11) DEFAULT NULL,
  `cash_or_card` int(11) DEFAULT NULL COMMENT 'キャッシュは1、カードは2',
  `day` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `log`
--

INSERT INTO `log` (`staff_id`, `guest_id`, `goods_id`, `goods_num`, `price`, `cash_or_card`, `day`) VALUES
(1, 1, 1, 1, 3240, 1, '2014-06-24 02:41:48'),
(2, 2, 1, 1, 3240, 1, '2014-06-24 02:42:50'),
(2, 2, 2, 2, 4320, 1, '2014-06-24 02:42:50'),
(2, 1, 2, 2, 4320, 1, '2014-06-24 02:55:20'),
(2, 1, 2, 1, 2160, 1, '2014-06-24 03:16:35'),
(2, 1, 1, 1, 3240, 1, '2014-06-24 05:00:18'),
(2, 1, 1, 1, 3240, 1, '2014-06-25 19:09:12'),
(2, 1, 2, 1, 2160, 1, '2014-06-25 19:10:25'),
(2, 1, 1, 1, 3240, 1, '2014-06-25 19:12:25'),
(1, 1, 2, 1, 2160, 1, '2014-06-25 23:42:44'),
(1, 1, 2, 1, 2160, 2, '2014-06-26 16:58:04');

-- --------------------------------------------------------

--
-- テーブルの構造 `members`
--

DROP TABLE IF EXISTS `members`;
CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL,
  `login_id` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `members`
--

INSERT INTO `members` (`id`, `login_id`, `password`, `name`, `created`, `modified`) VALUES
(1, 'kuwana', 'kuwana', 'イオン桑名アンク店', '0000-00-00 00:00:00', '2014-06-23 10:27:26'),
(2, 'yokkaichi', 'yokkaichi', 'アピタ四日市店', '0000-00-00 00:00:00', '2014-06-23 10:30:22');

-- --------------------------------------------------------

--
-- テーブルの構造 `shop_log`
--

DROP TABLE IF EXISTS `shop_log`;
CREATE TABLE IF NOT EXISTS `shop_log` (
  `staff_id` int(11) DEFAULT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL,
  `goods_num` int(11) DEFAULT NULL COMMENT '個数',
  `price` int(11) DEFAULT NULL,
  `cash_or_card` int(11) DEFAULT NULL COMMENT 'キャッシュは1、カードは2',
  `day` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `shop_log`
--

INSERT INTO `shop_log` (`staff_id`, `guest_id`, `goods_id`, `goods_num`, `price`, `cash_or_card`, `day`) VALUES
(1, 1, 1, 1, 3240, 1, '2014-06-24 02:41:48'),
(2, 2, 1, 1, 3240, 1, '2014-06-24 02:42:50'),
(2, 2, 2, 2, 4320, 1, '2014-06-24 02:42:50'),
(2, 1, 2, 2, 4320, 1, '2014-06-24 02:55:20'),
(2, 1, 2, 1, 2160, 1, '2014-06-24 03:16:35'),
(2, 1, 1, 1, 3240, 1, '2014-06-24 05:00:18'),
(2, 1, 1, 1, 3240, 1, '2014-06-25 19:09:12'),
(2, 1, 2, 1, 2160, 1, '2014-06-25 19:10:25'),
(2, 1, 1, 1, 3240, 1, '2014-06-25 19:12:25'),
(1, 1, 2, 1, 2160, 1, '2014-06-25 23:42:44'),
(1, 1, 2, 1, 2160, 2, '2014-06-26 16:58:04'),
(1, 1, 1, 1, 3240, 1, '2014-06-27 20:02:12'),
(3, 1, 101004, 1, 540, 1, '2014-06-28 14:48:11'),
(3, 1, 101001, 1, 1080, 1, '2014-06-28 14:48:11'),
(3, 1, 2000001, 1, 2160, 1, '2014-06-28 14:48:11');

-- --------------------------------------------------------

--
-- テーブルの構造 `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `name` varchar(20) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `result` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `staff`
--

INSERT INTO `staff` (`name`, `id`, `result`) VALUES
('高田優人', 2, 0),
('油谷　創', 1, 0),
('高田国男', 3, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
